# Test suite for the Raspberry Pi Pico device

import os
from ast import literal_eval

# Add import path outside of current working directory
import sys
sys.path.insert(0, "/app")
import list_ports, pyboard

# Initial module configuration
def setup_module():
    COMPort = list_ports.getPortByVIDPID(int(os.getenv("RPI_PICO_VID"), 16), int(os.getenv("RPI_PICO_PID"), 16))
    assert not COMPort.startswith("No port found associated with")
    return pyboard.Pyboard(COMPort, 115200)

def lookup(pyb, command):
    return pyb.exec("print(" + command + ")").decode("utf-8").strip()

def test_implementation(pyb):
    pyb.enter_raw_repl()
    pyb.exec("import sys")
    print(lookup(pyb, "sys.implementation"))
    assert lookup(pyb, "sys.implementation.name") == "micropython"
    assert lookup(pyb, "sys.implementation._machine") == "Raspberry Pi Pico with RP2040"
    assert lookup(pyb, "sys.implementation.version") == "(1, 19, 1)"
    pyb.exit_raw_repl()

if __name__ == "__main__":
    pyb = setup_module()
    test_implementation(pyb)