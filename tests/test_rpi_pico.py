# Test suite for the Raspberry Pi Pico device

import os
import pytest
import xunitparser

# Add import path outside of current working directory
import sys
sys.path.insert(0, "/app")
import list_ports, pyboard

# Initial module configuration
def setup_module():
    COMPort = list_ports.getPortByVIDPID(int(os.getenv("RPI_PICO_VID"), 16), int(os.getenv("RPI_PICO_PID"), 16))
    assert not COMPort.startswith("No port found associated with")
    pytest.global_variable_1 = pyboard.Pyboard(COMPort, 115200)

# Helper function to return a lookup on the target device
def lookup(target_device, command):
    return target_device.exec("print(" + command + ")").decode("utf-8").strip()

# Test for name, machine, and version details
def test_implementation():
    pytest.global_variable_1.enter_raw_repl()
    pytest.global_variable_1.exec("import sys")
    print("\n[DEBUG]: sys.implementation = " + lookup(pytest.global_variable_1, "sys.implementation"))
    assert lookup(pytest.global_variable_1, "sys.implementation.name") == "micropython"
    assert lookup(pytest.global_variable_1, "sys.implementation._machine") == "Raspberry Pi Pico with RP2040"
    assert lookup(pytest.global_variable_1, "sys.implementation.version") == "(1, 19, 1)"
    pytest.global_variable_1.exit_raw_repl()