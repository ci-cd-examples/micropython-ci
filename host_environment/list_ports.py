from serial.tools import list_ports

# Assumes you have only one device plugged in with the 
# VID and PID specified
def getPortByVIDPID(VID, PID):
    for port in list_ports.comports():
        if (port.vid == VID and port.pid == PID):
            return port.device

    print("No port found associated with VID:", VID, "and PID:", PID)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(prog = 'list_ports.py', description = 'List the port of a device based on the VID and PID')
    parser.add_argument('-v', '--vid', help="Vendor ID of device (in hex)")
    parser.add_argument('-p', '--pid', help="Product ID of device (in hex)")
    args = parser.parse_args()

    print(getPortByVIDPID(VID=int(args.vid, 16), PID=int(args.pid, 16)))