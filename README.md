# MicroPython CI
CI Examples for use with MicroPython

## Running from a Docker container
You can run all CI scripts once configuring your Docker container like so:
```
docker run \
--privileged \
--volume $(pwd):/code \
--workdir /code \
-e RPI_PICO_VID="2e8a" \
-e RPI_PICO_PID="0005" \
-it registry.gitlab.com/ci-cd-examples/micropython-ci sh
```

Now you can run the following Pytest like so:
```
python -m pytest -s tests/test_rpi_pico.py --junitxml=testLog.xml
```